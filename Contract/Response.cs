﻿using System.Runtime.Serialization;

namespace Contract
{
    [DataContract]
    public struct Response
    {
        [DataMember]
        public ResponseTypeCode Type;
        [DataMember]
        public ResponseCode Result;
        [DataMember]
        public byte[] Data;
    }
}