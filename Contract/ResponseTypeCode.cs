﻿namespace Contract
{
    public enum ResponseTypeCode : byte
    {
        RegisterResponse = 1,
        DateTimeSyncResponse = 2
    }
}