﻿using System.Runtime.Serialization;

namespace Contract
{
    [DataContract]
    public struct Command
    {
        [DataMember]
        public long Token;
        [DataMember]
        public CommandTypeCode Type;
        [DataMember]
        public byte[] Data;
    }
}