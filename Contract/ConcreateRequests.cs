﻿using System.Runtime.Serialization;

namespace Contract
{
    [DataContract]
    public struct RegisterRequest
    {
        [DataMember]
        public string UserGuid;  //идентификатор от пользователя
    }
}