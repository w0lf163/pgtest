﻿using System.Runtime.Serialization;

namespace Contract
{
    [DataContract]
    public struct RegisterResponse
    {
        [DataMember]
        public long UniqueServerToken; //связанный с UserGuid уникальный токен
    }
    [DataContract]
    public struct DateTimeSyncResponse
    {
        [DataMember]
        public long UtcNow; //текщуее время на сервере
    }
}