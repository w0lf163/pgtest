﻿using Contract;
using LiteNetLib;
using MessagePack;
using System;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace Client
{
    [DataContract]
    public struct WrongData
    {
        [DataMember]
        public int Field;
        [DataMember]
        public int Field2;
        [DataMember]
        public int Field3;
    }

    class Program
    {
        private static NetPeer _connection;
        const int Port = 9050;
        
        static async Task Main(string[] args)
        {
            var SendRequestCount = 0;
            var source = new CancellationTokenSource();
            Console.WriteLine("Start Client");
            Console.WriteLine("Please enter SendRequestCount");
            SendRequestCount = int.Parse(Console.ReadLine());
            
            var useUniq = Read_YesNo("Use uniq guid ?");
                
            EventBasedNetListener listener = new EventBasedNetListener();
            NetManager client = new NetManager(listener, "key");
            client.Start();
            
            _connection = client.Connect("localhost", Port);

            long uniqueServerToken;

            listener.NetworkErrorEvent += (point, code) => { Console.WriteLine($"Error connection {point}, code:{code}"); };
            listener.PeerDisconnectedEvent += (peer, info) => { Console.WriteLine($"Disconnect {peer}, reason:{info.Reason}"); };
            
            listener.NetworkReceiveEvent += (peer, reader) =>
                {
                    var response = MessagePackSerializer.Deserialize<Response>(reader.Data);
                    if (response.Result == ResponseCode.Success)
                    {
                        switch (response.Type)
                        {
                            case ResponseTypeCode.RegisterResponse:
                                uniqueServerToken = MessagePackSerializer.Deserialize<RegisterResponse>(response.Data)
                                    .UniqueServerToken;

                                RepeatTimeRequest(SendRequestCount, source, _connection, uniqueServerToken);

                                break;
                            case ResponseTypeCode.DateTimeSyncResponse:
                                Console.WriteLine("Current time: {0}", new DateTime(MessagePackSerializer.Deserialize<DateTimeSyncResponse>(response.Data).UtcNow).ToString("dd.MMMM.yyyy HH:mm:ss"));
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Result Code: {response.Result}");
                    }
                    
                };

            SendCommand(_connection, 0, default(int), 0);
            SendCommand(_connection, CommandTypeCode.Registration, new WrongData(), 0);

            SendCommand(_connection, CommandTypeCode.Registration,
                new RegisterRequest { UserGuid = useUniq ? Guid.NewGuid().ToString() : "NonUniqId" }, 0);

            while (!Console.KeyAvailable)
            {
                client.PollEvents();
                if (_connection.ConnectionState == ConnectionState.Disconnected)
                {
                    if (Read_YesNo("Reconnect?"))
                    {
                        _connection = client.Connect("localhost", Port);
                        SendCommand(_connection, CommandTypeCode.Registration,
                            new RegisterRequest { UserGuid = useUniq ? Guid.NewGuid().ToString() : "NonUniqId" }, 0);
                    }
                    else
                    {
                        break;
                    }
                }
                await Task.Delay(15);
            }
            source.Cancel();
            client.Stop();
        }

        private static void RepeatTimeRequest(int requestCount, CancellationTokenSource source, NetPeer connection,
            long token)
        {
            var single = true;
            Task.Run(async () =>
            {
                for (int i = 0; i < requestCount; i++)
                {
                    await Task.Delay(TimeSpan.FromSeconds(1), source.Token);
                    SendCommand(connection, CommandTypeCode.SyncTime, default(int), token);
                    if (single)
                    {
                        single = false;
                        SendCommand(connection, CommandTypeCode.SyncTime, default(int), token + 1);
                    }
                }
            }, source.Token);
        }

        public static void SendCommand<T>(NetPeer peer, CommandTypeCode code, T commandData, long token)
        {
            peer.Send(MessagePackSerializer.Serialize(new Command
            {
                Type = code,
                Token = token,
                Data = MessagePackSerializer.Serialize(commandData)
            }), SendOptions.Sequenced);
        }

        public static bool Read_YesNo(string message)
        {
            Console.WriteLine($"{message} Y/N");
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.Y:
                    Console.WriteLine();
                    return true;
                case ConsoleKey.N:
                    Console.WriteLine();
                    return false;
            }
            Console.WriteLine();
            return Read_YesNo(message);
        }
    }
}
