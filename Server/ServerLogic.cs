﻿using System;

namespace PGTest
{
    public class ServerLogic : IServerLogic
    {
        public long GetCurrentTimeInTicks()
        {
            return DateTime.UtcNow.Ticks;
        }
    }
}