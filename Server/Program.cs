﻿using LiteNetLib;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PGTest
{
    class Program
    {
        const int Port = 9050;
        const int MaxPeerNonActivityInSec = 2;

        static async Task Main(string[] args)
        {
            
            Console.WriteLine("Starting server ...");
            IAuthorizeService authorize = new AuthorizeService();
            var cmdHandler = new CommandsHandler(authorize,new ServerLogic());
            
            var listener = new EventBasedNetListener();
            var server = new NetManager(listener,100, "key");
            server.Start(Port);
            Console.WriteLine("Server started...");

            listener.PeerConnectedEvent += peer =>
            {
                Console.WriteLine("Peer connected: {0}", peer.EndPoint);
                authorize.CreateNewUser(peer.ConnectId);
            };

            listener.PeerDisconnectedEvent += (peer, info) =>
            {
                Console.WriteLine("Peer disconnected: {0}, reason: {1}", peer.EndPoint, info.Reason);
                authorize.UnRegisterUser(peer.ConnectId);
            };

            listener.NetworkReceiveEvent += (peer, reader) =>
            {
                cmdHandler.Handle(peer,reader);
            };

            var peers = new List<NetPeer>();
            while (!Console.KeyAvailable)
            {
                server.PollEvents();
                CheckPeersActivity(server, peers, authorize);
                await Task.Delay(15);
            }
            
            server.Stop();
            Console.WriteLine("Server stop...");
        }

        private static void CheckPeersActivity(NetManager server, List<NetPeer> peers, IAuthorizeService authorize)
        {
            server.GetPeersNonAlloc(peers);
            foreach (var netPeer in peers)
            {
                var user = authorize.GetUser(netPeer.ConnectId);
                if (user == null) continue;
                if (DateTime.UtcNow.Subtract(user.LastActivity).Seconds > MaxPeerNonActivityInSec) 
                {
                    authorize.UnRegisterUser(netPeer.ConnectId);
                    server.DisconnectPeer(netPeer);
                }
            }
        }
    }
}
