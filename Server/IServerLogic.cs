﻿
namespace PGTest
{
    public interface IServerLogic
    {
        long GetCurrentTimeInTicks();
    }
}