﻿using System;
using System.IO;
using Contract;
using LiteNetLib;
using LiteNetLib.Utils;
using MessagePack;


namespace PGTest
{
    public class CommandsHandler
    {
        private readonly IAuthorizeService _authorize;
        private readonly IServerLogic _logic;

        public CommandsHandler(IAuthorizeService authorize, IServerLogic logic)
        {
            _authorize = authorize;
            _logic = logic;
        }

        public void Handle(NetPeer peer, NetDataReader reader)
        {
            var cmd = MessagePackSerializer.Deserialize<Command>(reader.Data);
            try
            {
                switch (cmd.Type)
                {
                    case CommandTypeCode.Registration:
                        Registration(peer, cmd);
                        break;

                    case CommandTypeCode.SyncTime:
                        SyncTime(peer, cmd);
                        break;

                    default:
                        SendResponse(peer, ResponseCode.BadCommand);
                        break;
                }
            }
            catch (FormatterNotRegisteredException e)
            {
                SendResponse(peer, ResponseCode.WrongData);
                Console.WriteLine(e);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void SyncTime(NetPeer peer, Command cmd)
        {
            var state = _authorize.ValidateToken(peer.ConnectId, cmd.Token);
            switch (state)
            {
                case TokenState.Invalid:
                    SendResponse(peer, ResponseCode.WrongTokenError);
                    break;
                case TokenState.AlreadyExist:
                    SendResponse(peer, ResponseCode.MultipleClients);
                    break;
                case TokenState.Valid:
                    _authorize.GetUser(peer.ConnectId).LastActivity = DateTime.UtcNow;
                    SendResponse(peer, new DateTimeSyncResponse() { UtcNow = _logic.GetCurrentTimeInTicks() }, ResponseTypeCode.DateTimeSyncResponse);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void Registration(NetPeer peer, Command cmd)
        {
            var req = MessagePackSerializer.Deserialize<RegisterRequest>(cmd.Data);
            if (req.UserGuid == null)
            {
                SendResponse(peer, ResponseCode.WrongData);
                return;
            }

            var res = _authorize.RegisterUser(peer.ConnectId, req.UserGuid);
            if (res)
            {
                SendResponse(peer, new RegisterResponse() { UniqueServerToken = _authorize.GetUser(peer.ConnectId).Token }, ResponseTypeCode.RegisterResponse);
            }
        }

        private static void SendResponse<T>(NetPeer peer, T response, ResponseTypeCode code)
        {
            peer.Send(MessagePackSerializer.Serialize(new Response()
            {
                Type = code,
                Result = ResponseCode.Success,
                Data = MessagePackSerializer.Serialize(response)
            }), SendOptions.Sequenced);
        }

        private static void SendResponse(NetPeer peer, ResponseCode code)
        {
            peer.Send(MessagePackSerializer.Serialize(new Response()
            {
                Result = code
            }), SendOptions.Sequenced);
        }
    }
}